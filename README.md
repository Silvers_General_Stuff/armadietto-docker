Use this something like:


```
services:
  storage:
    build: https://gitlab.com/Silvers_General_Stuff/armadietto-docker.git
    ports:
      - "8000:8000"
    volumes:
      - ./remotestorage:/storage-data
```