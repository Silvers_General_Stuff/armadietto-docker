
const Armadietto = require('armadietto');

const store = new Armadietto.FileTree({path: '/storage-data'});

const server = new Armadietto({
    store: store,
    http: { host: '0.0.0.0', port: 8000 },
    allow: { signup: true }
});

server.boot();